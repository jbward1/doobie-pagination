
lazy val Http4sVersion = "0.21.0-M5"
lazy val CirceVersion = "0.12.2"
lazy val LogbackVersion = "1.2.3"
lazy val SilencerVersion = "1.4.4"
lazy val TapirVersion = "0.12.1"
lazy val ScribeVersion = "2.7.10"
lazy val DoobieVersion = "0.8.6"
lazy val ScalaTestVersion = "3.0.8"
lazy val AWSSDKVersion = "2.10.25"

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding",
  "UTF-8",
  "-language:higherKinds",
  "-language:postfixOps",
  "-feature",
  "-Xfatal-warnings",
)

lazy val root = (project in file("."))
  .settings(
    organization := "com.ward",
    name := "doobie-pagination",
    version := "0.0.1",
    scalaVersion := "2.13.1",
    libraryDependencies ++= Seq(
      //Logging
      "com.outr" %% "scribe" % ScribeVersion,
      "com.outr" %% "scribe-slf4j" % ScribeVersion,

      //Database
      "org.liquibase" % "liquibase-core" % "3.5.5",
      "org.tpolecat" %% "doobie-core" % DoobieVersion,
      "org.tpolecat" %% "doobie-postgres" % DoobieVersion,
      "org.tpolecat" %% "doobie-hikari" % DoobieVersion,

      //AWS
      "software.amazon.awssdk" % "ssm" % AWSSDKVersion,

      //Test
      "org.tpolecat" %% "doobie-scalatest" % DoobieVersion % Test,
      "com.opentable.components" % "otj-pg-embedded" % "0.13.3" % Test,
      "org.scalatest" %% "scalatest" % ScalaTestVersion % Test,
      "org.scalamock" %% "scalamock" % "4.4.0" % Test,
    ),
    addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1"),
    addCompilerPlugin("com.github.ghik" % "silencer-plugin" % SilencerVersion cross CrossVersion.full),
  )

