create user "test" with
    PASSWORD 'localtest'
    LOGIN
    NOSUPERUSER
    INHERIT
    CREATEDB
    CREATEROLE
    NOREPLICATION;

create database testdb with OWNER test;

\connect testdb;
create extension "pgcrypto";