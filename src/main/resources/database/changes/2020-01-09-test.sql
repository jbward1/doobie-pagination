--liquibase formatted sql

--changeset ward:create_test
create table test
(
    id uuid,
    name varchar(255)      not null,
    updated_at timestamptz not null default now(),
    constraint "datasets.dataset_series_pk" primary key (id)
);
--rollback drop table if exists test;