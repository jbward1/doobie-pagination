package com.ward.pagination

import java.time.Instant
import java.util.UUID

trait BaseDao {
  def id: UUID

  def updatedAt: Instant
}
