package com.ward.pagination

import doobie.free.connection.ConnectionIO
import doobie._
import doobie.implicits._

trait Pagination[Dao <: BaseDao, Query] {
  implicit def read: Read[Dao]

  private val DefaultLimit = 100

  protected def queryWithParams(queryParams: Query): Fragment

  def listWithPagination(
    queryParams: Query,
    limit: Int = DefaultLimit
  ): ConnectionIO[Vector[Dao]] = {

    (queryWithParams(queryParams) ++
     // Generic pagination logic goes here
      fr"limit $limit").query[Dao].stream.compile.toVector
  }
}