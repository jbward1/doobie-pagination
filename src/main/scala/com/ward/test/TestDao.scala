package com.ward.test

import java.time.Instant
import java.util.UUID

import com.ward.pagination.BaseDao

case class TestDao(id: UUID, name: String, updatedAt: Instant) extends BaseDao
