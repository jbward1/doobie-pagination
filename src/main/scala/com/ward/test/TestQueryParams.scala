package com.ward.test

import java.time.Instant

case class TestQueryParams(name: Option[String] = None, updatedAt: Option[Instant] = None)
