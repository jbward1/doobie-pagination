package com.ward.test

import com.ward.pagination.Pagination
import doobie._
import Fragments.whereAndOpt
import com.ward.util.SQLSyntax
import doobie.implicits._
import doobie.util.fragment.Fragment

trait TestRepository extends Pagination[TestDao, TestQueryParams] with SQLSyntax {
  implicit def read: Read[TestDao] = Read[TestDao]

  private def selectTest: Fragment = fr"select id, name, updated_at"

  private def fromTest: Fragment = fr"from test"

  protected def queryWithParams(queryParams: TestQueryParams): Fragment = {
    import queryParams._

    selectTest ++
      fromTest ++
      whereAndOpt(
        sqls.like("name", name),
        sqls.ge("updatedAt", updatedAt)
      )
  }
}
