package com.ward.util

import doobie.implicits._
import doobie.util.Put
import doobie.util.fragment.Fragment

trait SQLSyntax {
  val sqls: SQLSyntax.type = SQLSyntax
}

object SQLSyntax {

  def like[T: Put](columnName: String, value: Option[T]): Option[Fragment] =
    value.map(v => Fragment.const(s"$columnName like") ++ fr"$v")

  def ge[T: Put](columnName: String, value: Option[T]): Option[Fragment] =
    value.map(v => Fragment.const(s"$columnName >=") ++ fr"$v")
}
