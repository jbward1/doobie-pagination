package com.ward

import cats.effect.{ContextShift, IO}
import doobie.util.ExecutionContexts
import doobie.util.transactor.Transactor
import org.scalatest.FlatSpec

import scala.util.{Failure, Success}

trait DatabaseFlatSpec extends FlatSpec {

  implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContexts.synchronous)

  implicit val xa: Transactor[IO] = {
    EmbeddedPostgres.startupLocalDatabase match {
      case Success(dataSourceTransactor) => dataSourceTransactor
      case Failure(exception) =>
        fail(s"Failed to startup embedded database. ${exception.getMessage}")
    }
  }
}
