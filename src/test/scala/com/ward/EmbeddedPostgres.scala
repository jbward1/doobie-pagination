package com.ward

import cats.effect.{Blocker, ContextShift, IO}
import com.opentable.db.postgres.embedded.{EmbeddedPostgres => EmbeddedPostgresDatabase}
import doobie.util.ExecutionContexts
import doobie.util.transactor.Transactor
import javax.sql.DataSource

import scala.io.Source
import scala.util.Try

object EmbeddedPostgres {
  implicit val contextShift: ContextShift[IO] =
    IO.contextShift(ExecutionContexts.synchronous)

  lazy val startupLocalDatabase: Try[Transactor[IO]] = {
    for {
      dataSource <- startupPostgresDatabase
      _ <- runInitScript(dataSource)
      transactor <- createTransactor(dataSource)
      _ <- runLiquibaseScripts(transactor)
    } yield transactor
  }

  private lazy val startupPostgresDatabase = Try {
    EmbeddedPostgresDatabase
      .builder()
      .setPort(Config.LocalDatabasePort)
      .start
      .getPostgresDatabase
  }

  private def runInitScript(dataStore: DataSource) = Try {
    dataStore.getConnection.createStatement().execute(initScript)
  }

  private lazy val initScript = {
    val source = Source.fromFile("scripts/database/bootstrap/init.sql")
    val contents = source.getLines().filterNot(_.startsWith("\\connect")).mkString("\n")
    source.close()
    contents
  }

  private def runLiquibaseScripts(transactor: Transactor[IO]) = Try {
    LiquibaseSupport.runLiquibaseUpdate(transactor).unsafeRunSync()
  }

  def createTransactor(dataSource: DataSource): Try[Transactor[IO]] = Try {
    Transactor.fromDataSource[IO](
      dataSource,
      ExecutionContexts.synchronous,
      Blocker.liftExecutionContext(ExecutionContexts.synchronous)
    )
  }
}
