package com.ward

import cats.effect.IO
import doobie.free.connection
import doobie.free.connection.ConnectionIO
import doobie.util.transactor.Transactor
import liquibase.database.DatabaseFactory
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.ClassLoaderResourceAccessor
import liquibase.{Contexts, Liquibase}
import doobie.implicits._

object LiquibaseSupport {
  val changeLogFile: String = "database/change-log.xml"
  val liquibaseSchemaName: String = "public"

  private def updateDatabase(): ConnectionIO[Unit] = connection.raw { connection =>
    val resourceAccessor = new ClassLoaderResourceAccessor()
    val database = DatabaseFactory
      .getInstance()
      .findCorrectDatabaseImplementation(new JdbcConnection(connection))

    database.setLiquibaseSchemaName(liquibaseSchemaName)

    new Liquibase(changeLogFile, resourceAccessor, database)
      .update(new Contexts())
  }

  def runLiquibaseUpdate(transactor: Transactor[IO]): IO[Unit] =
    updateDatabase().transact(transactor)
}
