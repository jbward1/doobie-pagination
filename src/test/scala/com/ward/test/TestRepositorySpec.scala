package com.ward.test

import com.ward.DatabaseFlatSpec
import doobie.implicits._
import org.scalatest.Matchers

class TestRepositorySpec extends DatabaseFlatSpec with TestRepository with Matchers {

  "a test database" should "list and return no results" in {
    listWithPagination(TestQueryParams()).transact(xa).unsafeRunSync() shouldEqual Vector()
  }
}
